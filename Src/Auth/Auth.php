<?php

namespace App\Auth;
include ("../../vendor/autoload.php");
use App\Utility\Utility;
use PDO;
session_start();
class Auth
{
    private $username;
    private $email;
    private $password;

    public function login($data = '')
    {
        $this->username = "'" . $data['username']. "'";
        $this->password = "'" . $data['password']. "'";

        $pdo = new PDO('mysql:host=localhost;dbname=phpfirst', 'root', '');
        $query = "SELECT * FROM  `users` where username=$this->username AND password=$this->password";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data=$stmt->fetch();
        if(!empty($data)){
            $_SESSION['userid'] = $data;
            header('location:index.php');
        }else{
            $_SESSION['Message']="<h1>Opps! Username or Password Given Wrong.</h1>";
            header('location:login.php');
        }
    }
    public function setData($data = '')
    {
        $this->username = $data['username'];
        $this->password = $data['password'];
        $this->email = $data['email'];
        return $this;
    }

    public function getAllUsers()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=phpfirst', 'root', '');
            $query = "SELECT * FROM  `users` LIMIT 0 , 30";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data=$stmt->fetchAll();
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $data;
    }

    public function store()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=phpfirst', 'root', '');
            $query = "INSERT INTO users(id, username, password, email, created_at) VALUES (:i, :u, :p, :e, :created_at)";

            $stmt = $pdo->prepare($query);
            $status = $stmt->execute(
                array(
                    ':i' => null,
                    ':u' => $this->username,
                    ':p' => $this->password,
                    ':e' => $this->email,
                    ':created_at' => date('Y-m-d h:m:s'),
                )
            );
            if ($status) {
                $_SESSION['Message']="<h1>Signup Success.Login plz.</h1>";
                header('location:login.php');
            } else {
                echo "Something wrong";
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function show($id=''){
        $pdo = new PDO('mysql:host=localhost;dbname=phpfirst', 'root', '');
        $query = "SELECT * FROM  `users` where id=$id LIMIT 0 , 30";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data=$stmt->fetch();
        return $data;
    }
}