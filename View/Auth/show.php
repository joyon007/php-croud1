<?php
include("../../vendor/autoload.php");
use App\Auth\Auth;
use App\Utility\Utility;
$obj=new Auth();
$myData=$obj->show($_GET['id']);
?>

<table border="1">
    <tr>
        <td>ID</td>
        <td>User Name</td>
        <td>Email</td>
        <td>Member Since</td>
    </tr>
        <tr>
            <td><?php echo $myData['id']?></td>
            <td><?php echo $myData['username'] ?></td>
            <td><?php echo $myData['email'] ?></td>
            <td><?php echo $myData['created_at'] ?></td>
        </tr>
</table>
