<?php
session_start();
if(!empty($_SESSION['userid'])){
    unset($_SESSION['userid']);
    $_SESSION['Message'] = "Logout Success";
    header('location:login.php');
}